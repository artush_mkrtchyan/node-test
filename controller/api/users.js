const { Users } = require('../../models/users');
const { getError, hashPassword } = require('../../lib/utils');
const { validateDbObjectId } = require('../../lib/validate');

user = async (req, res, next) => {
    try {
        const err = validateDbObjectId(req.params.id);
        if (err) {
            return next(err);
        }

        const user = await Users.findById(req.params.id).select(
            'firstName lastName',
        );
        if (!user) {
            throw getError(404, 'User Not Found.');
        }
        return res.json(user);
    } catch (e) {
        return next(e);
    }
};

users = async (req, res, next) => {
    try {
        const users = await Users.find().select('firstName lastName');
        return res.json(users);
    } catch (e) {
        return next(e);
    }
};

create = async (req, res, next) => {
    try {
        const { username, password, firstName } = req.body;

        if (!username || !password || !firstName) {
            return res.status(422).json({
                message: 'firstName, username and password required',
            });
        }

        if (username) {
            const user = await Users.findOne({ username });
            if (user) {
                return res.status(422).json({
                    status: 409,
                    message: 'User Name already exists.',
                });
            }
        }

        const newUser = new Users({
            ...req.body,
            password: hashPassword(password),
        });

        const user = await newUser.save();

        return res
            .status(201)
            .json({
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
            });
    } catch (e) {
        return next(e);
    }
};

update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const err = validateDbObjectId(id);
        if (err) {
            return next(err);
        }

        const user = await Users.findByIdAndUpdate(
            id,
            { $set: { ...req.body } },
            { new: true },
        ).select('firstName lastName');

        return res.json(user);
    } catch (e) {
        return next(e);
    }
};

deleteUser = async (req, res, next) => {
    try {
        const { id } = req.params;
        const err = validateDbObjectId(id);
        if (err) {
            return next(err);
        }

        const user = await Users.findByIdAndDelete(id);
        if (!user) {
            return next(getError(404, 'User Not Found.'));
        }

        return res.json({ deleted: true });
    } catch (e) {
        return next(e);
    }
};

findUsers = async (find) => {
    try {
        return await Users.find(find).select('-password');
    } catch (e) {
        throw e;
    }
};

module.exports = {
    user,
    users,
    create,
    update,
    deleteUser,
    findUsers,
};
