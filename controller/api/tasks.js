const { Tasks } = require('../../models/tasks');
const { getError } = require('../../lib/utils');
const { validateDbObjectId } = require('../../lib/validate');
const { POSITION } = require('../../lib/variables');
const { findUsers } = require('./users');

tasks = async (req, res, next) => {
    try {
        const find = {};

        if (req.user.position === POSITION.manager) {
            const users = await findUsers({ manager: req.user.id });
            find.assignee = {
                $in: [req.user.id, ...users.map((el) => el._id)],
            };
        } else {
            find.assignee = req.user.id;
        }

        const tasks = await Tasks.find(find).populate(
            'assignee',
            'firstName lastName',
        );
        return res.json(tasks);
    } catch (e) {
        return next(e);
    }
};

task = async (req, res, next) => {
    try {
        const err = validateDbObjectId(req.params.id);
        if (err) {
            return next(err);
        }

        const task = await Tasks.findById(req.params.id);
        if (!task) {
            throw getError(404, 'Task Not Found.');
        }
        return res.json(task);
    } catch (e) {
        return next(e);
    }
};

create = async (req, res, next) => {
    try {
        const body = { ...req.body, attachment: req.body.file };
        delete body.file;
        const newTask = new Tasks(body);
        const task = await newTask.save();

        return res.status(201).json(task);
    } catch (e) {
        return next(e);
    }
};

update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const err = validateDbObjectId(id);
        if (err) {
            return next(err);
        }
        const body = { ...req.body, attachment: req.body.file };
        const task = await Tasks.findByIdAndUpdate(
            id,
            { $set: body },
            { new: true },
        );
        return res.json(task);
    } catch (e) {
        return next(e);
    }
};

deleteTask = async (req, res, next) => {
    try {
        const { id } = req.params;
        const err = validateDbObjectId(id);
        if (err) {
            return next(err);
        }
        const task = await Tasks.findByIdAndDelete(id);
        if (!task) {
            return next(getError(404, 'Task Not Found.'));
        }

        return res.json({ deleted: true });
    } catch (e) {
        return next(e);
    }
};

module.exports = {
    task,
    tasks,
    create,
    update,
    deleteTask,
};
