const app = require('express')();

app.use('/users', require('./v1/users'));
app.use('/tasks', require('./v1/tasks'));

module.exports = app;
