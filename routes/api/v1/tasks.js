const router = require('express').Router();
const TasksController = require('../../../controller/api/tasks');
const {
    isAuthorized,
    isPositionManager,
} = require('../../../middlewares/auth');
const { uploadFileMiddleware } = require('../../../middlewares/upload');

router.get('/', isAuthorized, TasksController.tasks);

router.get('/:id', isAuthorized, TasksController.task);

router.post(
    '/',
    [isAuthorized, isPositionManager, uploadFileMiddleware],
    TasksController.create,
);

router.put(
    '/:id',
    [isAuthorized, isPositionManager, uploadFileMiddleware],
    TasksController.update,
);

router.delete(
    '/:id',
    isAuthorized,
    isPositionManager,
    TasksController.deleteTask,
);

module.exports = router;
