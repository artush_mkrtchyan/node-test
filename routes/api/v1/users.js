const router = require('express').Router();
const UsersController = require('../../../controller/api/users');
const { isAuthorized } = require('../../../middlewares/auth');

router.get('/', isAuthorized, UsersController.users);

router.get('/:id', isAuthorized, UsersController.user);

router.post('/', UsersController.create);

router.put('/:id', isAuthorized, UsersController.update);

router.delete('/:id', isAuthorized, UsersController.deleteUser);

module.exports = router;
