const mongoose = require('mongoose');

const tasksSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            trim: true,
        },
        assignee: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
        },
        deadline: {
            type: Date,
            trim: true,
        },
        attachment: {
            type: String,
        },
    },
    { timestamps: true },
);

exports.Tasks = mongoose.model('Tasks', tasksSchema);