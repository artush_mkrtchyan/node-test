const mongoose = require('mongoose');
const { ENUM_POSITION, POSITION } = require('../lib/variables');

const usersSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            required: true,
            trim: true,
        },
        lastName: {
            type: String,
            trim: true,
        },
        username: {
            type: String,
            trim: true,
            uniq: true,
            required: true,
        },
        password: {
            type: String,
            minlength: 6,
            maxlength: 1024,
            required: true,
        },
        position: {
            type: String,
            default: POSITION.employee,
            enum: ENUM_POSITION,
        },
        manager: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
        },
    },
    { timestamps: true },
);

const Users = mongoose.model('Users', usersSchema);

exports.Users = Users;
