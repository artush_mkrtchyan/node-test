const multer = require('multer');
const util = require('util');
const path = require('path');
const { getError, createNewFolder } = require('../lib/utils');

const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;
const day = date.getDate();
const uploadPath = `uploads/${year}/${month}/${day}`;
const dir = path.join(__dirname, `../public/${uploadPath}`);

const getFileName = (file) => `${date.getTime()}_${file.originalname}`;

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, dir);
    },
    filename: (req, file, cb) => {
        cb(null, getFileName(file));
    },
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype.startsWith('application/') ||
        file.mimetype.startsWith('image') ||
        file.mimetype.startsWith('text/')
    ) {
        cb(null, true);
    } else {
        cb('Please upload only correct files.', false);
    }
};

const upload = multer({ storage, fileFilter }).single('file');

const uploadFileMiddleware = util.promisify((req, res, next) => {
    createNewFolder(dir);
    return upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return next(getError(500, err));
        } else if (err) {
            return next(getError(500, err));
        }

        if (req.file) {
            req.body.file = '/' + uploadPath + '/' + getFileName(req.file);
        }
        return next();
    });
});

module.exports = {
    uploadFileMiddleware,
};
