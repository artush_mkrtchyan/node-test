const { Users } = require('../models/users');
const { getBasicAuthorizationData, checkPassword } = require('../lib/utils');
const { POSITION } = require('../lib/variables');

exports.isAuthorized = async (req, res, next) => {
    try {
        const { authorization } = req.headers;
        if (!authorization || authorization.split(' ')[0] !== 'Basic') {
            return res
                .status(401)
                .json({ message: 'Missing Authorization Header' });
        }

        const [username, password] = getBasicAuthorizationData(
            authorization.split(' ')[1],
        );

        const user = await Users.findOne({ username });

        if (user && checkPassword(password, user.password)) {
            req.user = {
                id: user._id,
                position: user.position,
                manager: user.manager,
            };
            return next();
        }

        return res.status(401).json({ message: 'Wrong username or password' });
    } catch (error) {
        next(e);
    }
};

exports.isPositionManager = (req, res, next) => {
    const { position } = req.user;
    if (position !== POSITION.manager) {
        return res.status(403).json({ message: 'Permission denied.' });
    }
    return next();
};

exports.isCurrentUser = (req, res, next) => {
    const { id } = req.user;
    if (id !== req.params.id) {
        return res.status(403).json({ message: 'Permission denied.' });
    }
    return next();
};
