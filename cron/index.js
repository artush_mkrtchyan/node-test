const cron = require('node-cron');
const { Tasks } = require('../models/tasks');

cron.schedule('0 0 * * *', async () => {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const tasks = await Tasks.find({
        deadline: {
            $gte: date,
            $lte: new Date(),
        },
    }).populate('assignee', 'firstName');

    /*
        const emails = tasks.map((item) => item.assignee.email);
        sendEmailsFunction(emails)
    */
});
