const ENUM_POSITION = ['employee', 'manager'];
const POSITION = {
    manager: 'manager',
    employee: 'employee',
};

module.exports = {
    ENUM_POSITION,
    POSITION,
};
