const mongoose = require('mongoose');

function validateDbObjectId(id) {
    const isValid = mongoose.Types.ObjectId.isValid(id);
    if (!isValid) {
        const err = new Error('Id is not a valid id');
        err.status = 400;
        return err;
    }
    return '';
}

module.exports = {
    validateDbObjectId,
};
