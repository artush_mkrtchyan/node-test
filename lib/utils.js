const bcrypt = require('bcrypt');
const fs = require('fs')

function getError(status, message) {
    const err = new Error(message);
    err.status = status;
    return err;
}
function getBasicAuthorizationData(credentials) {
    try {
        return Buffer.from(credentials, 'base64').toString().split(':');
    } catch (error) {
        return [];
    }
}
function hashPassword(password) {
    return bcrypt.hashSync(password, 10);
}

function checkPassword(password, hash) {
    if (!password || !hash) return false;
    return bcrypt.compareSync(password, hash);
}
function createNewFolder(dir) {
    try {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        return dir;
    } catch (err) {
        console.error(err);
    }
}
module.exports = {
    getError,
    getBasicAuthorizationData,
    hashPassword,
    checkPassword,
    createNewFolder,
};
